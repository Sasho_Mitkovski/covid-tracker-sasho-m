<?php

$pdo = new PDO("mysql:host=localhost;dbname=covid-tracker", 'root', '');

//DDOS denial of service

$sql = "SELECT * FROM countries";
$stmt = $pdo->query($sql);
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

    $data = file_get_contents("https://api.covid19api.com/total/dayone/country/" . $row['slug']);

    // echo $data;
    $data = json_decode($data, true);
    $country_id = $row['id'];

    $sql2 = "INSERT INTO cases (country_id, active, deaths, recovered, confirmed, `date`)
            VALUES(:country_id, :active, :deaths, :recovered, :confirmed, :date)";

    $stmt2 = $pdo->prepare($sql2);
    $stmt2->bindParam("country_id", $country_id);
    $stmt2->bindParam("active", $active);
    $stmt2->bindParam("deaths", $deaths);
    $stmt2->bindParam("recovered", $recovered);
    $stmt2->bindParam("confirmed", $confirmed);
    $stmt2->bindParam("date", $date);

    foreach ($data as $item) {
        $active = $item['Active'];
        $deaths = $item['Deaths'];
        $confirmed = $item['Confirmed'];
        $recovered = $item['Recovered'];
        $date = date("Y-m-d", strtotime($item['Date']));


        $stmt2->execute();
    }
}
