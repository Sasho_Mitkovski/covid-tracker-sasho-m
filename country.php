<?php
$pdo = new PDO("mysql:host=localhost;dbname=covid-tracker", 'root', '');

$sql = "SELECT * FROM countries";
$stmt = $pdo->query($sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>country</title>
</head>

<body>
<div class="container-fluid mt-4">
<div class="row">
<div class="col-md-10">
 <h3 class="text-center">Tabela so podacoci od site drzavi</h3>
</div>
<div class="col-md-2">
<a href="index.php" class="btn btn-secondary w-100">Vrati se nazad</a>
</div>
</div>
    <table class="w-100 table table-dark table-hover">
        <tr class="text-warning border fw-bolder">
            <td>Country</td>
            <td>Active</td>
            <td>Deaths</td>
            <td>Recovered</td>
            <td>Confirmed</td>
            <td>New active</td>
            <td>New deaths</td>
            <td>New recovered</td>
            <td>New confirmed</td>
        </tr>
        <?php
        while ($row = $stmt->fetch()) {
            echo "<tr><td>{$row['name']}</td>";
            $sql_cases_today = "SELECT * FROM cases WHERE country_id = '{$row['id']}' ORDER BY `date` DESC LIMIT 1";
            $stmt_cases_today = $pdo->query($sql_cases_today);
            $row_cases_today = $stmt_cases_today->fetch();

            if ($stmt_cases_today->rowCount() == 0) {
                echo "<td colspan='8'>No info</td>";
            } else {
                $todayActive = $row_cases_today['active'];
                $todayDeaths = $row_cases_today['deaths'];
                $todayRecovered = $row_cases_today['recovered'];
                $todayConfirmed = $row_cases_today['confirmed'];

                $sql_cases_yesterday = "SELECT * FROM cases WHERE country_id = '{$row['id']}' ORDER BY `date` DESC LIMIT 1 OFFSET 1";
                $stmt_cases_yesterday = $pdo->query($sql_cases_yesterday);
                $row_cases_yesterday = $stmt_cases_yesterday->fetch();
                if ($stmt_cases_yesterday->rowCount() == 0) {
                    $newActive = "/";
                    $newDeaths = "/";
                    $newConfirmed = "/";
                    $newRecovered = "/";
                } else {
                    $yesterdayActive = $row_cases_yesterday['active'];
                    $yesterdayDeaths = $row_cases_yesterday['deaths'];
                    $yesterdayConfirmed = $row_cases_yesterday['confirmed'];
                    $yesterdayRecovered = $row_cases_yesterday['recovered'];

                    $newActive = $todayActive - $yesterdayActive;
                    $newConfirmed = $todayConfirmed - $yesterdayConfirmed;
                    $newDeaths = $todayDeaths - $yesterdayDeaths;
                    $newRecovered = $todayRecovered - $yesterdayRecovered;
                }

                echo "<td>$todayActive</td><td>$todayDeaths</td><td>$todayRecovered</td><td>$todayConfirmed</td>";
                echo "<td>$newActive</td><td>$newDeaths</td><td>$newRecovered</td><td>$newConfirmed</td>";
            }
            echo "</tr>";
        }
        ?>
    </table>
    </div>
</body>

</html>