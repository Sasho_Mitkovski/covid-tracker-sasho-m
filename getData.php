<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('db.php');

    if ($_POST['filter'] == 'today') {
        $first_date = date('Y-m-d', strtotime('-1 day'));
        $second_date = date('Y-m-d', strtotime('-2 days'));
    } else if ($_POST['filter'] == 'monthly') {
        $first_date = date('Y-m-d', strtotime('-1 day'));
        $second_date = date('Y-m-d', strtotime('-1 month'));
    } else {
        $first_date = date('Y-m-d', strtotime('-1 day'));
        $second_date = date('Y-m-d', strtotime('-2 months'));
    }

    $country_filter = '';
    if ($_POST['country_id'] != '-1') {
        $country_filter = ' and country_id=' . $_POST['country_id'];
    }

    $sql = "select (SELECT sum(confirmed) FROM cases where date = '" . $first_date . "'" . $country_filter . ") as today, 
(select sum(confirmed) FROM cases where date = '" . $second_date . "'" . $country_filter . ") as yesterday
from cases";
    $stmt = $pdo->query($sql);
    $total_confirmed = $stmt->fetch();

    $sql_deaths = "select (SELECT sum(deaths) FROM cases where date = '" . $first_date . "'" . $country_filter . ") as today, 
            (select sum(deaths) FROM cases where date = '" . $second_date . "'" . $country_filter . ") as yesterday
            from cases";
    $stmt_deaths = $pdo->query($sql_deaths);
    $total_deaths = $stmt_deaths->fetch();

    $sql_recovered = "select (SELECT sum(recovered) FROM cases where date = '" . $first_date . "'" . $country_filter . ") as today, 
            (select sum(recovered) FROM cases where date = '" . $second_date . "'" . $country_filter . ") as yesterday
            from cases";
    $stmt_recovered = $pdo->query($sql_recovered);
    $total_recovered = $stmt_recovered->fetch();

    $return_data = [
        'confirmed_today' => $total_confirmed['today'],
        'confirmed_yesterday' => $total_confirmed['yesterday'],
        'deaths_today' => $total_deaths['today'],
        'deaths_yesterday' => $total_deaths['yesterday'],
        'recovered_today' => $total_recovered['today'],
        'recovered_yesterday' => $total_recovered['yesterday'],
    ];

    echo json_encode($return_data);
}
