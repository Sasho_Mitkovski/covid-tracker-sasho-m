<?php
require('db.php');
$sql = "select * from countries order by name";
$stmt = $pdo->query($sql);
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
        <link rel="stylesheet" href="style.css">
        <title>Covid-19 / Proect 2</title>
    </head>

    <body>

        <img src="./img/world.jpg" class="position-fixed">
        <div class="container-fluid blur">

            <div class="row  p-4">
                <div class="col-md-9 text-white  ">
                    <h3 class="text-center my-3">Informacii za covid-19 na nivo na cel svet</h3>

                </div>
                <div class="col-md-3">
                    <label for="sync">Sinhronizacija </label>
                    <button type="button" id="sync" class="btn btn-primary form-control ">Sinhroniziraj</button>
                </div>
            </div>
            <div class="row p-4">
                <div class="col-md-3 text-white">
                    <div class="row">
                        <form class="form-group">
                            <div class="col">
                            </div>
                        
                            <div class="col">
                                <div class="col ">
                                    <label<small>Tabela so site drzavi</small></label>
                                    <a href="country.php" class="btn btn-primary form-control width-100">Tabela</a>
                                </div>
                            </div>
                            <div class="col mt-4">
                                <label<small>Informacii za denes</small></label>
                                <a class="btn btn-primary form-control width-100" href="javascript:getData('today')">Denes</a>
                            </div>
                            <div class="col mt-4">
                                <label><small>Informacii za posledniot mesec dena</small></label>
                                <a class="btn btn-primary form-control width-100" href="javascript:getData('monthly')">Posleden
                                    mesec</a>
                            </div>
                            <div class="col mt-4">
                                <label<small>Informacii za poslednite tri meseci</small></label>
                                <a class="btn btn-primary form-control width-100" href="javascript:getData('threemonthly')">Posledni tri
                                    meseci</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Tabela -->
                <div class="col-md-9 border text-white" id="table_info">
                <div class="form-group mt-3">
                <label for="">Izberi drzava</label>
                        <select name="country_id" id="country_id" class="form-control bg-success text-light">
                            <option value="-1">All countries</option>
                            <?php while ($row = $stmt->fetch()) { ?>
                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <h4 class="text-center my-3 today-text">Informacii za deneska:
                        <?php echo date("Y/m/d") ?>
                    </h4>
                    <h4 class="text-center my-3 monthly-text" style="display:none">
                        Posledniot mesec
                    </h4>
                    <h4 class="text-center my-3 threemonthly-text" style="display:none">
                        Poslednite 3 meseci
                    </h4>
                  
              
                    <div class="linija "></div>
                    <div class="row">
                        <!-- card -->
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger shadow  rounded">
                                <div class="card-header">
                                    NewConfirmed
                                </div>
                                <div class="card-body">
                                    <p class="card-text new-today"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger">
                                <div class="card-header">
                                    NewDeaths
                                </div>
                                <div class="card-body">
                                    <p class="card-text deaths-today"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger">
                                <div class="card-header">
                                    NewRecovered
                                </div>
                                <div class="card-body">
                                    <p class="card-text recovered-today"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- card 2 1 -->
                    <h4 class="text-center my-3">Sevkupni informacii za cel svet</h4>
                    <div class="linija "></div>
                    <div class="row">
                        <!-- card -->
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger shadow  rounded">
                                <div class="card-header">
                                    TotalConfirmed
                                </div>
                                <div class="card-body">
                                    <p class="card-text new-total"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger">
                                <div class="card-header">
                                    TotalDeaths
                                </div>
                                <div class="card-body">
                                    <p class="card-text deaths-total"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 p-2">
                            <div class="card text-white text-center bg-danger">
                                <div class="card-header">
                                    TotalRecovered
                                </div>
                                <div class="card-body">
                                    <p class="card-text recovered-total"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
            <div class="row mt-2 m-1">

                <div class="col">
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                    <h4 class="text-light">Graficki prikaz</h4>
                    <figure class="highcharts-figure">
                        <div id="container"></div>
                        <p class="highcharts-description">
                            Highcharts has extensive support for time series, and will adapt
                            intelligently to the input data. Click and drag in the chart to zoom in
                            and inspect the data.
                        </p>
                    </figure>
                </div>
            </div>
        </div>



        <!--   Analiser   -->
     
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
        </script>

        <script src="./js/js-code.js"></script>
        <script src="chart.js"> </script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    </body>

    </html>