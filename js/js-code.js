function getData(date = 'today') {
    $.ajax({
        url: "getData.php",
        method: 'post',
        data: {
            filter: date,
            country_id: $('#country_id').val()
        },
        success: function(data) {
            data = $.parseJSON(data);
            $('.new-today').text(data['confirmed_today'] - data['confirmed_yesterday'])
            $('.deaths-today').text(data['deaths_today'] - data['deaths_yesterday'])
            $('.recovered-today').text(data['recovered_today'] - data['recovered_yesterday'])
            $('.new-total').text(data['confirmed_today'])
            $('.deaths-total').text(data['deaths_today'])
            $('.recovered-total').text(data['recovered_today'])

            if (date == 'today') {
                $('.today-text').show();
                $('.monthly-text').hide();
                $('.threemonthly-text').hide();
            } else if (date == 'monthly') {
                $('.today-text').hide();
                $('.monthly-text').show();
                $('.threemonthly-text').hide();
            } else {
                $('.today-text').hide();
                $('.monthly-text').hide();
                $('.threemonthly-text').show();
            }
        },
        error: function() {
            alert("There was an error.");
        }
    });
}

$(document).ready(function() {
    getData();
});
$('#country_id').on('change', function() {
    getData();
})